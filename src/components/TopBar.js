import React from 'react';

import { Anchor, Header, Nav } from 'grommet';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';

const items = [
  { label: 'Company', href: '/' },
  { label: 'Employee Login', href: '/login' },
];

const TopBar = props => {
  const { cookies } = props;
  const empid = cookies.get('emid');

  return (
    <Header background="white" pad="small">
      <Nav direction="row">
        {items.map((item, key) => {
          console.log(key, empid);
          if (key === 1 && empid) {
            return;
          }
          return <Anchor href={item.href} label={item.label} key={item.label} color="brand" />;
        })}
        {/* {empid ? (
          <Anchor
            label="Logout"
            color="brand"
            onClick={() => {
              cookies.remove('emid');
              history.push('/');
            }}
          />
        ) : null} */}
      </Nav>
    </Header>
  );
};

export default withRouter(withCookies(TopBar));
