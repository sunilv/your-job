import React from 'react';

import {
  Anchor,
  Box,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHeader,
  TableRow,
  Text,
} from 'grommet';
import { FormDown, Notification, FormUp } from 'grommet-icons';

const data = [
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: True,
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: True,
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: True,
  },
  {
    name: 'sunil',
    mobile: '0909090909',
    is_active: True,
  },
];
const Row = ({ row }) => (
  <TableRow>
    {row.map(c => (
      <TableCell key={c.property} scope="col" align={c.align}>
        <Text>{c.label}</Text>
      </TableCell>
    ))}
  </TableRow>
);

const TableView = () => {
  return (
    <Box align="center" pad="large">
      <Table caption="Default Table">
        <TableHeader>{<Row row={['Name', 'Mobile', 'Is Active', 'Actions']} />}</TableHeader>
        <TableBody>
          {data.map((datum, key) => (
            <Row row={datum} key={`table-${key}`} />
          ))}
        </TableBody>
      </Table>
    </Box>
  );
};

export default TableView;
