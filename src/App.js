import React from 'react';
import { Box, Text } from 'grommet';

import TopBar from './components/TopBar';

function App(props) {
  return (
    <div>
      <Box
        width="100%"
        height={`100vh`}
        pad={0}
        style={{
          overflow: 'scroll',
        }}
      >
        <TopBar />
        {props.children}
      </Box>
    </div>
  );
}

export default App;
