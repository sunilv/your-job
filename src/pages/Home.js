import React, { Component } from 'react';
import { Box, Heading, Text, Anchor, TextInput, Button } from 'grommet';

import { withRouter } from 'react-router-dom';
import { CaretNext, Add, Link, Map, Group, TreeOption, LinkNext, Search } from 'grommet-icons';
import { company } from '../constants/data';
import TopBar from '../components/TopBar';

import styled from 'styled-components';

const Tag = styled.label`
  background: #87ceeb;
  padding: 10px;
  color: black;
  border-radius: 5px;
  position: absolute;
  right: 10px;
`;

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      index: 0,
      company,
    };
  }

  onActive = nextIndex => {
    this.setState({
      index: nextIndex,
    });
  };

  componentDidMount() {}

  /** Search Company */
  onSearchCompany = () => {
    const { query } = this.state;
    var result = company.filter(com => {
      return com.name.toLowerCase().indexOf(query.toLowerCase()) > -1;
    });

    this.setState({
      company: result,
      loading: false,
    });
  };

  onSearchTextChange = val => {
    this.setState(
      {
        query: val,
      },
      () => this.onSearchCompany(),
    );
  };

  render() {
    const { company, query } = this.state;
    return (
      <Box
        style={{
          position: 'relative',
        }}
      >
        <TopBar />
        <Box
          margin="medium"
          style={{
            position: 'absolute',
            right: 50,
            top: 50,
          }}
        >
          <Button icon={<Add />} label="New Company" primary />
        </Box>
        <Box margin="medium" align="center">
          <Box direction="row" align="center">
            <Heading level="4">Companies All over the world</Heading>
            <CaretNext />
          </Box>
          <Box justify="center" alignSelf="center" width="medium" margin="small">
            <TextInput
              icon={<Search />}
              placeholder="Search Company ..."
              value={query}
              onChange={e => this.onSearchTextChange(e.target.value)}
              style={{
                background: 'white',
                border: 0,
              }}
            />
          </Box>
          <Box direction="row-responsive" wrap>
            {company.length ? (
              company.map((com, key) => {
                return (
                  <Box
                    margin="small"
                    pad="medium"
                    background="white"
                    style={{ position: 'relative' }}
                  >
                    <Heading level="3">{com.name}</Heading>

                    <Box direction="row" align="center">
                      <Map />
                      <Text>{com.location}</Text>
                    </Box>
                    <Box direction="row" margin={{ vertical: 'small' }}>
                      <Anchor
                        color="#444444"
                        icon={<Link size="20px" />}
                        label={com.website}
                        href={`https:${com.website}`}
                        target="_blank"
                      />
                    </Box>
                    <Box direction="row" justify="between" gap="medium">
                      <Box direction="row" gap="medium">
                        <Box direction="row" gap="small">
                          <Group />
                          <Text>{com.employees}</Text>
                        </Box>
                        <Box direction="row" gap="small">
                          <TreeOption />
                          <Text>Branches</Text>
                          <Text>{com.branches.length}</Text>
                        </Box>
                      </Box>
                      <Anchor
                        label="More about us"
                        icon={<LinkNext />}
                        reverse
                        href={`/${com.id}/dashboard`}
                      />
                    </Box>

                    <Tag>{com.category} </Tag>
                  </Box>
                );
              })
            ) : (
              <Box height="small" width="small">
                No Companies Found :({' '}
              </Box>
            )}
          </Box>
        </Box>
      </Box>
    );
  }
}

export default withRouter(Home);
