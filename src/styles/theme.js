import { css } from 'styled-components';

import {
  primary,
  secondary,
  tertiary,
  quartenary,
  background,
  backgrounder,
  black,
  light,
} from './colors';
import { COLORS } from '.';

const APP_COLORS = {
  brand: primary,
  secondary,
  tertiary,
  quartenary,
  background,
  backgrounder,
};

export const PROPS = {
  global: {
    colors: APP_COLORS,
    font: {
      family: 'Ubuntu',
      size: '15px',
    },
    edgeSize: {
      small: '10px',
    },
    elevation: {
      light: {
        small: '0px 1px 5px rgba(0, 0, 0, 0.50)',
        medium: '0px 3px 8px rgba(0, 0, 0, 0.50)',
      },
    },
  },
  button: {
    border: {
      radius: '5px',
    },
  },

  text: {
    medium: {
      size: '15px',
    },
  },
  tab: {
    active: {
      background,
      color: secondary,
    },
    background: 'white',
    border: undefined,
    color: black,
    hover: {
      color: quartenary,
    },
    margin: undefined,
    pad: {
      vertical: 'xsmall',
      horizontal: 'small',
    },
    extend: ({ theme }) => css`
      border-radius: 10px;
    `,
  },
  tabs: {
    background: 'white',

    gap: 'medium',
    margin: {
      medium: 'small',
    },
    header: {
      background: 'white',
      extend: ({ theme }) => css`
        padding: ${theme.global.edgeSize.small};
        border-bottom-color: #f4f5f8;
        border-bottom-width: 2px;
        border-bottom-style: solid;
      `,
    },
    panel: {
      extend: ({ theme }) => css`
        padding: 0;
      `,
    },
  },
  table: {
    header: {
      border: undefined,
    },
  },
};
