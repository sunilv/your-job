import * as THEME from './theme';
import * as COLORS from './colors';

export { THEME, COLORS };
