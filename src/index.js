import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Grommet } from 'grommet';

import { grommet } from 'grommet/themes';
import { deepMerge } from 'grommet/utils';
import { THEME } from './styles';
import { CookiesProvider } from 'react-cookie';

import Protected from './utils/Protected';
import NotFound from './pages/PageNotFound';
import Home from './pages/Home';
import CompanyDashboard from './pages/CompanyDashboard';
import EmployeeDashboard from './pages/EmployeeDashboard';
import Login from './pages/Login';

const customTheme = deepMerge(grommet, THEME.PROPS);

const Main = () => (
  <Grommet full theme={customTheme}>
    <CookiesProvider>
      <Router>
        <Switch>
          <Protected path="/employee/:empId" exact component={EmployeeDashboard} />
          <Route path="/" exact component={Home} />
          <Route path="/:companyId/dashboard" exact component={CompanyDashboard} />
          <Route path="/login" exact component={Login} />
          <Route path="/not-found" component={NotFound} />
          <Route path="*" component={NotFound} />
        </Switch>
      </Router>
    </CookiesProvider>
  </Grommet>
);

ReactDOM.render(<Main />, document.getElementById('root'));
