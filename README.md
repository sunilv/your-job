# Your Job portal

Built in ReactJS

## Installation

```bash
git clone <url>
cd your-job
yarn

# Start server for development
yarn start
```

## Features Completed

### Company View

- Company Home page - list view, public access, employee list.(previous employee info included)

### Employee View

- Employee login, employee home page, private access
